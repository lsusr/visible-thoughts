Onto logistics.

The most important thing to making this project a success is for me to understand precisely what constitutes a quality run. If I understand what constitutes a quality run then I can work quickly without ever having to bother you. This is the single biggest bottleneck to scaling production of this dataset.

Until now, I have always submitted my authors' work to you unedited. I had an important reason for this: In the early days of this project, I would tell authors to change lots of things that didn't need to be changed. (I changed more things that didn't matter than things that did.) It is only by submitting their unedited work to you that I learned what didn't matter.

I think that from now on, whenever an author writes a run, I should instead edit it as best as I can myself before submitting it to you. This way you will no longer be reviewing the work of my authors. You will be reviewing my work (except lillalira, who has proven herself). This should be a step toward shifting the burden of editing from you to me. Once I am consistently delivering quality work to you we can finally delegate your job to me.
